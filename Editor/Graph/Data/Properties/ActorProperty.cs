using System;
using DialogueSystem;
using UnityEngine;

namespace DialogueSystem.Editor {
    [Serializable]
    public class ActorProperty : AbstractProperty {
        public ActorProperty() {
            DisplayName = "Actor";
            Type = PropertyType.Actor;
        }

        public override AbstractProperty Copy() {
            return new ActorProperty {
                DisplayName = DisplayName,
                Hidden = Hidden
            };
        }
    }

}