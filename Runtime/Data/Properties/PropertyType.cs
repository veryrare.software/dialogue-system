namespace DialogueSystem {
    public enum PropertyType {
        Trigger,
        Check,
        Actor
    }
}