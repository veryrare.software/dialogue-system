
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DialogueSystem
{
    [AddComponentMenu("Dialogue Graph/Dialogue Graph")]
    public partial class RuntimeDialogueGraph : MonoBehaviour {
        public DlogObject DlogObject;

        #region Inspector Data
        public string CurrentAssetGuid;
        public List<DlogObjectData> PersistentData = new List<DlogObjectData>();
        public StringIntSerializableDictionary PersistentDataIndices = new StringIntSerializableDictionary();
        public DlogObjectData CurrentData {
            get {
                if (string.IsNullOrEmpty(CurrentAssetGuid)) return null;
                if (!PersistentDataIndices.ContainsKey(CurrentAssetGuid)) {
                    PersistentDataIndices[CurrentAssetGuid] = PersistentData.Count;
                    PersistentData.Add(new DlogObjectData());
                }

                return PersistentData[PersistentDataIndices[CurrentAssetGuid]];
            }
        }
        public int CurrentIndex {
            get {
                if (string.IsNullOrEmpty(CurrentAssetGuid)) return -1;
                if (!PersistentDataIndices.ContainsKey(CurrentAssetGuid)) {
                    PersistentDataIndices[CurrentAssetGuid] = PersistentData.Count;
                    PersistentData.Add(new DlogObjectData());
                }

                return PersistentDataIndices[CurrentAssetGuid];
            }
        }
        public void ClearData() {
            PersistentDataIndices = new StringIntSerializableDictionary();
            PersistentData = new List<DlogObjectData>();
        }
        #endregion

        [System.Serializable]
        public class StringBoolDictionary : SerializableDictionary<string, bool>{} 
        public StringBoolDictionary boolVars;

        public UnityEvent<List<ConversationLine>, float, bool> OnUpdateLines;
        public UnityEvent OnConversationDone;

        private bool conversationDone;
        private string currentNodeGuid;

        public void ResetConversation() {
            conversationDone = false;
            currentNodeGuid = DlogObject.StartNode;
        }

        public void EndConversation() {
            conversationDone = true;
            currentNodeGuid = null;
        }

        public bool IsCurrentNpc() {
            var currentNode = DlogObject.NodeDictionary[currentNodeGuid];
            return currentNode.Type == NodeType.NPC;
        }

        public bool IsConversationDone() {
            return conversationDone;
        }

        public ActorData GetCurrentActor() {
            var currentNode = DlogObject.NodeDictionary[currentNodeGuid];
            if (currentNode.Type != NodeType.NPC) return null;
            var currentNodeActorGuid = currentNode.ActorGuid;
            var actor = CurrentData.ActorData[CurrentData.ActorDataIndices[currentNodeActorGuid]];
            return actor;
        }

        public List<ConversationLine> GetCurrentLines() {
            var currentNode = DlogObject.NodeDictionary[currentNodeGuid];
            return currentNode.Lines;
        }

        public float GetCurrentTimeLimit() {
            var currentNode = DlogObject.NodeDictionary[currentNodeGuid];
            return currentNode.timeLimit;
        }

        bool isReset = false;

        public void OnOpen(List<KeyValuePair<string, bool>> newBoolVars) {
            OnUpdateBoolVars(newBoolVars);
            if (!isReset) ResetConversation();
            OnProgress();
        }

        public void OnUpdateBoolVars(List<KeyValuePair<string, bool>> newBoolVars)
        {
            foreach(var v in newBoolVars)
            {
                var b = boolVars[v.Key] = v.Value;
            }
        }

        public void ProgressNpc() {
            var lines = GetCurrentLines();
            for (var i = 0; i < lines.Count; i++) {
                var line = lines[i];
                line.isEnabled = ExecuteChecks(line, i);
                if (line.isEnabled) 
                {
                    Progress(line);
                    ExecuteTriggers(line, i);
                    OnProgress();
                    return;
                }
            }
            Debug.LogError("NO POSSIBLE LINE IN NPC NODE");
        }

        public void ProgressSelf(int lineIndex) {
            var lines = GetCurrentLines();
            var line = lines[lineIndex];
            var isEnabled = ExecuteChecks(line, lineIndex);
            if ( !isEnabled) return;

            Progress(lines[lineIndex]);
            ExecuteTriggers(lines[lineIndex], lineIndex);
            OnProgress();
        }

        void OnProgress()
        {
            if ( IsConversationDone() )
            {
                OnConversationDone.Invoke();
                return;
            }
            var currentNode = DlogObject.NodeDictionary[currentNodeGuid];
            var isNPC = currentNode.Type == NodeType.NPC;
            var lines = currentNode.Lines;
            for (var i = 0; i < lines.Count; i++) 
                lines[i].isEnabled = ExecuteChecks(lines[i], i);
            OnUpdateLines.Invoke(lines, currentNode.timeLimit, isNPC );
        }

        bool ExecuteChecks(ConversationLine line, int lineIndex) {
            bool currentCheck = true;
            foreach (CheckTree tree in line.CheckTrees) {
                currentCheck = EvaluateCheckTree(tree, lineIndex) && currentCheck;
            }
            return currentCheck;
        }

        void ExecuteTriggers(ConversationLine line, int lineIndex) {
            Property loopTrigger = null;
            foreach (var triggerGuid in line.Triggers) {
                // todo check trigger name
                var triggerProperty = DlogObject.Properties.Find(property => property.Type == PropertyType.Trigger && property.Guid == triggerGuid);
                if ( triggerProperty.DisplayName.StartsWith("SetBoolFalse_") )
                    boolVars["Bool_" + triggerProperty.DisplayName.Replace("SetBoolFalse_", "")] = false;
                else if ( triggerProperty.DisplayName.StartsWith("SetBoolTrue_") )
                    boolVars["Bool_" + triggerProperty.DisplayName.Replace("SetBoolTrue_", "")] = true;
                else if (loopTrigger == null && triggerProperty != null && triggerProperty.DisplayName.StartsWith("Loop_"))
                    loopTrigger = triggerProperty;
                else 
                    CurrentData.TriggerData[CurrentData.TriggerDataIndices[triggerGuid]].Invoke();
            }
            if ( loopTrigger != null )
            {
                var loopNodeName = loopTrigger.DisplayName.Replace("Loop_","");
                string guid = null;
                foreach(var n in DlogObject.NodeDictionary )
                {
                    if ( n.Value.nodeName == loopNodeName)
                    {
                        guid = n.Key;
                        break;
                    }
                }
                if ( guid == null) 
                {
                    return;
                }
                conversationDone = false;
                currentNodeGuid = guid;
            }
        }

        void Progress(ConversationLine line) {
            if (string.IsNullOrEmpty(line.Next)) {
                conversationDone = true;
                currentNodeGuid = null;
                return;
            }
            currentNodeGuid = line.Next;
        }

        bool EvaluateCheckTree(CheckTree tree, int lineIndex) {
            if (tree.NodeKind == CheckTree.Kind.Property) {
                if (string.IsNullOrEmpty(tree.PropertyGuid)) return false;
                if (!CurrentData.CheckDataIndices.ContainsKey(tree.PropertyGuid)) return false;
                int index = CurrentData.CheckDataIndices[tree.PropertyGuid];
                if (index < 0 || index >= CurrentData.CheckData.Count) return false;

                var prop = DlogObject.Properties.Find(x=>x.Guid == tree.PropertyGuid);
                if (prop == null) return false;
                Debug.Log("Evaluate chekc tree:" + prop.DisplayName);
                return boolVars.GetValueOrDefault(prop.DisplayName, false);
                // return CurrentData.CheckData[index].Invoke(currentNodeGuid, lineIndex);
            }

            if (tree.NodeKind == CheckTree.Kind.Unary) {
                bool check = EvaluateCheckTree(tree.SubtreeA, lineIndex);
                return EvaluateUnaryOperation(tree.BooleanOperation, check);
            }

            if (tree.NodeKind == CheckTree.Kind.Binary) {
                bool checkA = EvaluateCheckTree(tree.SubtreeA, lineIndex);
                bool checkB = EvaluateCheckTree(tree.SubtreeB, lineIndex);
                return EvaluateBinaryOperation(tree.BooleanOperation, checkA, checkB);
            }
            throw new Exception("Unreachable");
        }

        private static bool EvaluateUnaryOperation(BooleanOperation operation, bool value) {
            switch (operation) {
                case BooleanOperation.NOT: return !value;
                default: throw new Exception("Unreachable");
            }
        }

        private static bool EvaluateBinaryOperation(BooleanOperation operation, bool valueA, bool valueB) {
            switch (operation) {
                case BooleanOperation.AND: return valueA && valueB;
                case BooleanOperation.OR: return valueA || valueB;
                case BooleanOperation.XOR: return valueA ^ valueB;
                case BooleanOperation.NAND: return !(valueA && valueB);
                case BooleanOperation.NOR: return !(valueA || valueB);
                case BooleanOperation.XNOR: return !(valueA ^ valueB);
                default: throw new Exception("Unreachable");
            }
        }
    
        public void OnValidate()
        {
            SyncBools();
        }

        public void SyncBools()
        {
            var boolVarsTmp = new StringBoolDictionary();
            foreach(var prop in DlogObject.Properties)
                if ( prop.Type == PropertyType.Check)
                    boolVarsTmp.Add(prop.DisplayName, boolVars.GetValueOrDefault(prop.DisplayName, false));
            boolVars = boolVarsTmp;
        }

        public void SetBoolTrue(string key) { boolVars[key] = true; }
        public void SetBoolFalse(string key){ boolVars[key] = false; }
    }
}