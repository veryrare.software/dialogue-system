using System;
using UnityEngine;

namespace DialogueSystem.Editor {
    [Serializable]
    public struct NodeDrawState {
        [SerializeField] public Rect Position;
        [SerializeField] public bool Expanded;
    }
}