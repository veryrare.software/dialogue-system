using UnityEngine;

namespace DialogueSystem
{

    [System.Serializable]
    public class SerializablePair<T0, T1> 
    {
        [SerializeField] public T0 Key;
        [SerializeField] public T1 Value;

        public SerializablePair()
        {
        }

        public SerializablePair(T0 key, T1 value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
    
}