
using System.Collections.Generic;
using UnityEngine.UIElements;

namespace DialogueSystem.Editor {

    public static class ExtraFieldsNodeHelper
    {
        public static VisualElement GenerateExtraFields(AbstractNode n, string branchPortViewDataKey, int index)
        {
            var wrapper = new VisualElement();
            wrapper.AddToClassList("extra-fields");
            List<LineData> Lines = n is NpcNode npc ? npc.Lines : (n as SelfNode).Lines;

            var scrollSpeedField = new UnityEditor.UIElements.FloatField("Scroll"); 
            scrollSpeedField.AddToClassList("scroll-speed-field");
            scrollSpeedField.SetValueWithoutNotify(Lines[index].scrollSpeed);

            var moodField = new UnityEngine.UIElements.TextField("Mood");
            moodField.AddToClassList("mood-field");
            moodField.SetValueWithoutNotify(Lines[index].mood);

            var audioField = new UnityEditor.UIElements.ObjectField();
            audioField.objectType = typeof(UnityEngine.AudioClip);
            audioField.AddToClassList("audio-field");
            audioField.SetValueWithoutNotify(UnityEditor.AssetDatabase.LoadAssetAtPath ( UnityEditor.AssetDatabase.GUIDToAssetPath (Lines[index].audioGuid), typeof(UnityEngine.AudioClip)) as UnityEngine.AudioClip);

            var animField = new UnityEngine.UIElements.TextField("Anim");
            animField.AddToClassList("anim-trigger-field");
            animField.SetValueWithoutNotify(Lines[index].animTrigger);

            var textAlignField = new UnityEditor.UIElements.EnumField("Align", TextAlign.LEFT);
            textAlignField.AddToClassList("anim-trigger-field");
            textAlignField.SetValueWithoutNotify(Lines[index].textAlign);

            scrollSpeedField.RegisterCallback<FocusOutEvent>(evt => {
                var lineIndex = Lines.FindIndex(data => data.PortGuidA == branchPortViewDataKey);
                if (scrollSpeedField.value != Lines[lineIndex].scrollSpeed) {
                    n.Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Dialogue ScrollSpeed");
                    Lines[lineIndex].scrollSpeed = scrollSpeedField.value;
                }
            });
            moodField.RegisterCallback<FocusOutEvent>(evt => {
                var lineIndex = Lines.FindIndex(data => data.PortGuidA == branchPortViewDataKey);
                if (moodField.value != Lines[lineIndex].mood) {
                    n.Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Dialogue Mood");
                    Lines[lineIndex].mood = moodField.value;
                }
            });
            audioField.RegisterValueChangedCallback(evt => {
                var lineIndex = Lines.FindIndex(data => data.PortGuidA == branchPortViewDataKey);
                n.Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Dialogue AudioClip");
                if (UnityEditor.AssetDatabase.TryGetGUIDAndLocalFileIdentifier(audioField.value, out string guid, out long localId)) {
                    Lines[lineIndex].audioGuid = guid;
                } else 
                    Lines[lineIndex].audioGuid = null;
            });
            animField.RegisterCallback<FocusOutEvent>(evt => {
                var lineIndex = Lines.FindIndex(data => data.PortGuidA == branchPortViewDataKey);
                if (animField.value != Lines[lineIndex].animTrigger) {
                    n.Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Dialogue AnimTrigger");
                    Lines[lineIndex].animTrigger = animField.value;
                }
            });

            textAlignField.RegisterCallback<FocusOutEvent>(evt => {
                var lineIndex = Lines.FindIndex(data => data.PortGuidA == branchPortViewDataKey);
                if ((TextAlign)textAlignField.value != Lines[lineIndex].textAlign) {
                    n.Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Dialogue ScrollSpeed");
                    Lines[lineIndex].textAlign = (TextAlign)textAlignField.value;
                }
            });
            wrapper.Add(scrollSpeedField);
            wrapper.Add(moodField);
            wrapper.Add(animField);
            wrapper.Add(audioField);
            wrapper.Add(textAlignField);
            return wrapper;
        }
    }
}