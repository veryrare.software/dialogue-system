using System;

namespace DialogueSystem.Editor {
    public class DlogWindowEvents {
        public Action SaveRequested;
        public Func<bool> SaveAsRequested;
        public Action ShowInProjectRequested;
    }
}