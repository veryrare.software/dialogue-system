namespace DialogueSystem {
    public enum BooleanOperation {
        // Unary
        NOT,

        // Binary
        AND,
        OR,
        XOR,
        NAND,
        NOR,
        XNOR,
    }
}