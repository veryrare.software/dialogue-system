using System;
using System.Linq;
using DialogueSystem;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEditor.Experimental.GraphView;
using System.Collections.Generic;
using UnityEngine.UIElements;


namespace DialogueSystem.Editor {
    public class CommentNode : AbstractNode {
        public string commentVal;
        private TextField textField;

        public override void InitializeNode(EdgeConnectorListener edgeConnectorListener)
        {
            base.InitializeNode(edgeConnectorListener);

            textField = new TextField();
            textField.multiline = true;
            textField.AddToClassList("comment-text-field");

            textField.RegisterCallback<FocusOutEvent>(evt => {
                Owner.EditorView.DlogObject.RegisterCompleteObjectUndo("Changed Comment");
                commentVal = textField.value;
                if ( textField.value == "") {
                    textField.SetValueWithoutNotify("Comment");
                    textField.AddToClassList("placeholder");
                }
            });
            textField.RegisterCallback<FocusInEvent>(evt => {
                if ( textField.value == "Comment") 
                {
                    textField.SetValueWithoutNotify("");
                    textField.RemoveFromClassList("placeholder");
                }
            });

            textField.SetValueWithoutNotify(string.IsNullOrEmpty(commentVal) ? "Comment" : commentVal);
            if (string.IsNullOrEmpty(commentVal) ) textField.AddToClassList("placeholder");
            
            mainContainer.Add(textField);
            mainContainer.style.minWidth = 200;
            var titleElement = this.Q("title");
            titleElement.RemoveFromHierarchy();
        }


        public override string GetNodeData() {
            var root = new JObject();
            root["comment"] = commentVal;
            root.Merge(JObject.Parse(base.GetNodeData()));
            return root.ToString(Formatting.None);
        }

        public override void SetNodeData(string jsonData) {
            if(string.IsNullOrEmpty(jsonData)) return;

            base.SetNodeData(jsonData);
            var root = JObject.Parse(jsonData);
            commentVal = root.Value<string>("comment");
            textField.SetValueWithoutNotify(string.IsNullOrEmpty(commentVal) ? "Comment" : commentVal);
            if (string.IsNullOrEmpty(commentVal) ) textField.AddToClassList("placeholder");
            else textField.RemoveFromClassList("placeholder");
        }

    }
}