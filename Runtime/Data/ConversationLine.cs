using System;
using System.Collections.Generic;

namespace DialogueSystem {
    [Serializable]
    public enum TextAlign { DEFAULT = 0, LEFT = 1, CENTER = 2, RIGHT = 3} 
   
    [Serializable]
    public class ConversationLine {
        public string Message;
        
        public string mood;
        public string animationTrigger;
        public UnityEngine.AudioClip audio;
        public bool scrollText;
        public float scrollSpeed;
        public TextAlign textAlign;
        
        public bool isEnabled;
        
        public string actor;

        public string Next;
        public string TriggerPort;
        public string CheckPort;
        public List<string> Triggers;
        public List<string> Checks;
        public List<CheckTree> CheckTrees;
    }
}