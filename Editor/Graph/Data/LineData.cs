
namespace DialogueSystem.Editor {
    
    public class LineData {
        public string Line;
        public string PortGuidA;
        public string PortGuidB;
        public string PortGuidC;

        public string audioGuid;
        public float scrollSpeed;
        public string mood;
        public string animTrigger;
        public TextAlign textAlign = TextAlign.DEFAULT;
    }
}