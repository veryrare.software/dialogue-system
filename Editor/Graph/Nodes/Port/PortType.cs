using System;

namespace DialogueSystem.Editor {
    public enum PortType {
        Check,
        Trigger,
        Actor,
        Branch,
        Boolean,
        Fake
    }
}