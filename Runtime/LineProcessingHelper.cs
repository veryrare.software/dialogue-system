using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DialogueSystem {

    public static class LineProcessingHelper 
    {
        public static void ProcessLine(string message, TMPro.TMP_Text text,
            float scrollSpeed = 0f,
            float AngleMultiplier = 2.0f,
            float SpeedMultiplier = 2.0f,
            float CurveScale = 2.0f,
            float shakyAnimWaitTime = 0.1f
        )
        {
            var shakeIndexes = new List<Vector2Int>();
            var origMessage = AllIndexesOfKeywords(message, "<shaky>", "</shaky>", shakeIndexes);
            var doShakes = shakeIndexes.Count > 0;
            var doScroll = scrollSpeed > 0.01f;
            text.text = doScroll ? "" :origMessage;
            ShakyAnimRef shakyAnimRef = new ShakyAnimRef();
            if ( doShakes )
            {
                text.ForceMeshUpdate();
                shakyAnimRef.cachedMeshInfo = text.textInfo.CopyMeshInfoVertexData();
                shakyAnimRef.shakyIndexes = shakeIndexes;
                text.StartCoroutine(AnimateVertexColors(text, shakyAnimRef, AngleMultiplier, SpeedMultiplier, CurveScale, shakyAnimWaitTime));
            }

            if ( doScroll)
            {
                text.StartCoroutine(DoScroll(text, shakyAnimRef, origMessage, doShakes, scrollSpeed));
            }
        }

        static IEnumerator DoScroll(TMPro.TMP_Text text, ShakyAnimRef shakyAnimRef, string origMessage, bool doShakes, float scrollSpeed)
        {
            var scrollIdx = 0;
            while(text != null && text.isActiveAndEnabled && (scrollIdx < origMessage.Length) )
            {
                // else process once
                int tagLen = 1;
                if ( origMessage[scrollIdx] == '<') 
                {
                    var endTagIdx = origMessage.IndexOf('>', scrollIdx);
                    if ( endTagIdx != -1) tagLen = Mathf.Min(origMessage.Length-2,endTagIdx) - scrollIdx;
                }
                yield return new WaitForSecondsRealtime(1f / scrollSpeed);
                if ( tagLen > 1)
                {
                    text.text += origMessage.Substring(scrollIdx, tagLen);
                    scrollIdx += tagLen;
                }
                text.text += origMessage[scrollIdx++];
                if ( doShakes )
                {
                    text.ForceMeshUpdate();
                    shakyAnimRef.cachedMeshInfo = text.textInfo.CopyMeshInfoVertexData();
                }
            }
            text.text = origMessage;
        }

        static IEnumerator AnimateVertexColors(
            TMPro.TMP_Text text, 
            ShakyAnimRef shakyAnimRef,
            float AngleMultiplier = 2.0f,
            float SpeedMultiplier = 2.0f,
            float CurveScale = 2.0f,
            float shakyAnimWaitTime = 0.1f
        )
        {
            // We force an update of the text object since it would only be updated at the end of the frame. Ie. before this code is executed on the first frame.
            // Alternatively, we could yield and wait until the end of the frame when the text object will be generated.

            // Cache the vertex data of the text object as the Jitter FX is applied to the original position of the characters.
            while (text.textInfo != null && text.isActiveAndEnabled)
            {
                TMP_TextInfo textInfo = text.textInfo;

                // If No Characters then just yield and wait for some text to be added
                if (textInfo.characterCount == 0)
                {
                    yield return new WaitForSecondsRealtime(0.25f);
                    continue;
                }
                foreach(var shaky in shakyAnimRef.shakyIndexes)
                {
                    var end = Mathf.Min(shaky.y, textInfo.characterCount);  
                    for (int i = shaky.x; i < end; i++)
                    {
                        TMP_CharacterInfo charInfo = textInfo.characterInfo[i];
                        // Skip characters that are not visible and thus have no geometry to manipulate.
                        if (!charInfo.isVisible)
                            continue;

                        // Retrieve the pre-computed animation data for the given character.
                        VertexAnim vertAnim = vertexAnim[i];
                        // Get the index of the material used by the current character.
                        int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;
                        // Get the index of the first vertex used by this text element.
                        int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                        // Get the cached vertices of the mesh used by this text element (character or sprite).
                        Vector3[] sourceVertices = shakyAnimRef.cachedMeshInfo[materialIndex].vertices;
                        // Determine the center point of each character at the baseline.
                        //Vector2 charMidBasline = new Vector2((sourceVertices[vertexIndex + 0].x + sourceVertices[vertexIndex + 2].x) / 2, charInfo.baseLine);
                        // Determine the center point of each character.
                        Vector2 charMidBasline = (sourceVertices[vertexIndex + 0] + sourceVertices[vertexIndex + 2]) / 2;
                        // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                        // This is needed so the matrix TRS is applied at the origin for each character.
                        Vector3 offset = charMidBasline;

                        Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;

                        destinationVertices[vertexIndex + 0] = sourceVertices[vertexIndex + 0] - offset;
                        destinationVertices[vertexIndex + 1] = sourceVertices[vertexIndex + 1] - offset;
                        destinationVertices[vertexIndex + 2] = sourceVertices[vertexIndex + 2] - offset;
                        destinationVertices[vertexIndex + 3] = sourceVertices[vertexIndex + 3] - offset;

                        vertAnim.angle = Mathf.SmoothStep(-vertAnim.angleRange, vertAnim.angleRange, Mathf.PingPong(shakyAnimRef.loopCount / 25f * vertAnim.speed, 1f));
                        Vector3 jitterOffset = new Vector3(Random.Range(-.25f, .25f), Random.Range(-.25f, .25f), 0);

                        var matrix = Matrix4x4.TRS(jitterOffset * CurveScale, Quaternion.Euler(0, 0, Random.Range(-5f, 5f) * AngleMultiplier), Vector3.one);

                        destinationVertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 0]);
                        destinationVertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 1]);
                        destinationVertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 2]);
                        destinationVertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 3]);

                        destinationVertices[vertexIndex + 0] += offset;
                        destinationVertices[vertexIndex + 1] += offset;
                        destinationVertices[vertexIndex + 2] += offset;
                        destinationVertices[vertexIndex + 3] += offset;

                        vertexAnim[i] = vertAnim;
                    }
                }
                // Push changes into meshes
                for (int i = 0; i < textInfo.meshInfo.Length; i++)
                {
                    textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                    text.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
                }

                shakyAnimRef.loopCount += 1;

                yield return new WaitForSecondsRealtime(shakyAnimWaitTime);
            }
        }

        //helpers for shaky anim
        struct VertexAnim
        {
            public float angleRange;
            public float angle;
            public float speed;
        }

        static VertexAnim[] vertexAnim;

        static LineProcessingHelper()
        {
            // Create an Array which contains pre-computed Angle Ranges and Speeds for a bunch of characters.
            vertexAnim = new VertexAnim[1024];
            for (int i = 0; i < 1024; i++)
            {
                vertexAnim[i].angleRange = Random.Range(10f, 25f);
                vertexAnim[i].speed = Random.Range(1f, 3f);
            }
        }

        public static string AllIndexesOfKeywords(string str, string startKeyword, string endKeyword, List<Vector2Int> indexes) {
            int startKeywordLen = startKeyword.Length;
            int endKeywordLen = endKeyword.Length;
            int idx = 0;
            while( idx  < str.Length )
            {
                var startIdx = str.IndexOf(startKeyword, idx);
                if ( startIdx == -1 )
                {
                     // if no more shaky starts -> return found indexes;
                    break;
                }
                str = str.Remove(startIdx, startKeywordLen); // remove  keyword
                // end?
                var endIdx = str.IndexOf(endKeyword, idx);
                if ( endIdx == -1 )
                {
                    endIdx = str.Length;
                    indexes.Add(new Vector2Int(startIdx, endIdx));
                    break; // of no end keyword, add to string end
                }
                str = str.Remove(endIdx, endKeywordLen); // remove  end keyword
                indexes.Add(new Vector2Int(startIdx, endIdx));
                idx = endKeywordLen;
            }
            // return str.Replace(startKeyword, "").Replace(endKeyword, ""); // dont do it, coz it will mess up the indices
            return str;
        }


        public class ShakyAnimRef
        {
            public List<Vector2Int> shakyIndexes;
            public TMP_MeshInfo[] cachedMeshInfo;
            public int loopCount = 0;
        } 

    }
}
