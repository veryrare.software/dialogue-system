using System;
using System.Collections;
using System.Collections.Generic;
using DialogueSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DialogueSystem {


#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(DialogueUIController))]
public class BasicDialogueUIControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var sc = (target as DialogueUIController);
        if (GUILayout.Button("Open Dialogue")) sc.OpenDialogue(new List<KeyValuePair<string, bool>>());
    }
}
#endif

public class DialogueUIController : MonoBehaviour
{
    public UnityEvent OnConversationDone;
    public UnityEvent<List<KeyValuePair<string, bool>>> OnConversationOpen;
    public UnityEvent OnConversationReset;
    public UnityEvent<int> OnConversationProgressSelf;
    public UnityEvent OnConversationProgressNpc;
    public bool resetDialogueOnReopen;
    public bool overrideScrollSpeed;
    public float scrollSpeed;

    public RectTransform parent;
    public TMPro.TextMeshProUGUI linePrevious;
    public Button progressButton;
  
    public TMPro.TextMeshProUGUI lineSelf;
    public TMPro.TextMeshProUGUI lineSelfChoice;
    public TMPro.TextMeshProUGUI lineNpc;

    [Header("Runtime:")]
    public bool isNPC;
    public bool isOpen;
    public int selectedIndex;

    public virtual void OnUpdateLines(List<ConversationLine> lines, float timeLimit, bool isNPC)
    {
        if ( lines == null || lines.Count == 0)
        {
            return;
        }   
        this.isNPC = isNPC;

        // if its self line and there are prev lines and current has choices, copy the last ones text into the new one
        if (!isNPC && lines.Count > 1 && parent.childCount > 0)
        {
            string prevLine = parent.GetChild(parent.childCount-1).GetComponent<TMPro.TextMeshProUGUI>().text;
            linePrevious.text = prevLine;
            linePrevious.gameObject.SetActive(true);
        }
        else 
            linePrevious.gameObject.SetActive(false);

        // destroy prev ones
        for(int i = parent.childCount-1; i>=0; i--)
            Destroy(parent.GetChild(i).gameObject);

        for(int i = 0; i < lines.Count; i++)
        {
            var l = lines[i];
            if ( isNPC && !l.isEnabled ) continue; // if npc line and not isEnabled skip it
            var lineText = Instantiate(isNPC ? lineNpc : (lines.Count == 1 ? lineSelf : lineSelfChoice), parent);
            lineText.gameObject.SetActive(true);
            if ( l.textAlign == TextAlign.LEFT)
                lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Left;
            else if (l.textAlign == TextAlign.CENTER) 
                lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Center;
            else if (l.textAlign == TextAlign.RIGHT) 
                lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Right;
            // if is npc or only one line (no choice), just instantiate, otherwise also line number and add button callback
            if (isNPC || lines.Count == 1) 
            {
                LineProcessingHelper.ProcessLine(l.Message, lineText, overrideScrollSpeed ? scrollSpeed : l.scrollSpeed);
                break;
            }
            else 
            {
                LineProcessingHelper.ProcessLine((i+1)+". " + l.Message, lineText, overrideScrollSpeed ? scrollSpeed : l.scrollSpeed);
                var b = lineText.GetComponent<Button>();
                b.interactable = l.isEnabled;
                b.onClick.AddListener(()=>OnSelectClick(lineText));
            }
        }
        progressButton.gameObject.SetActive(isNPC || lines.Count <= 1);
        selectedIndex = -1;
        EventSystem.current.SetSelectedGameObject(null);
    }

    private void OnSelectClick(TMPro.TextMeshProUGUI lineText)
    {
        if (isNPC || parent.childCount <= 1) return;
        for(int i = 0; i < parent.childCount; i++)
        {
            if ( parent.GetChild(i) == lineText.transform) 
            {
                SelectLine(i);
                Progress();
            }
        }
    }

    public IEnumerator ScrollText(TMPro.TextMeshProUGUI text, float speed, string message, Action callbakc)
    {
        yield return null;
    }

    void Update()
    {
        if ( !isOpen) return;
        if (!isNPC && parent.childCount > 1)
        {
            if ( Input.GetKeyDown(KeyCode.Alpha1) && parent.childCount >= 1) SelectLine(0);
            if ( Input.GetKeyDown(KeyCode.Alpha2) && parent.childCount >= 2) SelectLine(1);
            if ( Input.GetKeyDown(KeyCode.Alpha3) && parent.childCount >= 3) SelectLine(2);
            if ( Input.GetKeyDown(KeyCode.Alpha4) && parent.childCount >= 4) SelectLine(3);
            if ( Input.GetKeyDown(KeyCode.Alpha5) && parent.childCount >= 5) SelectLine(4);
            if ( Input.GetKeyDown(KeyCode.Alpha6) && parent.childCount >= 6) SelectLine(5);
            if ( Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) ) SelectLine(selectedIndex - 1);
            if ( Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) ) SelectLine(selectedIndex + 1);
        }
        //Progress Press
        if ( Input.GetKeyDown(KeyCode.Space))
        {
            if (parent.childCount == 1 || (selectedIndex >= 0 && selectedIndex < parent.childCount)) Progress();
        }
    }

    void SelectLine(int index)
    {
        EventSystem.current.SetSelectedGameObject(null);
        selectedIndex = index;
        if ( selectedIndex < 0) selectedIndex = parent.childCount-1;
        else if ( selectedIndex > parent.childCount-1) selectedIndex = 0;
        if ( parent.GetChild(selectedIndex).TryGetComponent<Button>(out var b)) b.Select();
    }

    public void Progress()
    {
        if ( isNPC)
            OnConversationProgressNpc.Invoke();
        else if ( parent.childCount > 1)
            OnConversationProgressSelf.Invoke(selectedIndex);
        else
            OnConversationProgressSelf.Invoke(0);
    }
    
    [ContextMenu("OpenDialogue")]
    public void OpenDialogue(List<KeyValuePair<string, bool>> boolVars)
    {
        isOpen = true;
        if ( resetDialogueOnReopen ) OnConversationReset.Invoke();
        OnConversationOpen.Invoke(boolVars);
    }

    public void OnCloseDialogue()
    {
        isOpen = false;
        OnConversationDone.Invoke();
    }

}
}
